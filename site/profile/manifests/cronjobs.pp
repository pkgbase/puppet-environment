# a profile for configuring cron jobs
class profile::cronjobs (
  Hash $cronjob_defaults = {},
  Hash $cronjobs         = {},
){
  $cronjobs.each |$v, $d| {
    cron { $v:
      * => deep_merge($cronjob_defaults, $d),
    }
  }
}
