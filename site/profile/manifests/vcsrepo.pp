# vcsrepo
class profile::vcsrepo {
  $vcsrepo_defaults = lookup('profile::vcsrepo_defaults', Hash, undef, {})
  $vcsrepos = lookup('profile::vcsrepos', Hash, undef, {})

  $vcsrepos.each |$v, $d| {
    vcsrepo { $v:
      * => deep_merge($vcsrepo_defaults, $d),
    }
  }
}
