# installation & basic setup of *our* Apache httpd
class profile::apache (
  Hash $mdvhost_defaults = {},
  Hash $mdvhosts         = {},
  Hash $vhost_defaults   = {},
  Hash $vhosts           = {},
){
  $mod_md = $mdvhosts.empty ? {
    true  => [],
    false => [md, ssl, watchdog],
  }

  $default_mods = [
    alias,
    authn_core,
    autoindex,
    deflate,
    dir,
    expires,
    headers,
    $mod_md,
    mime,
    mime_magic,
    proxy,
    proxy_http,
    proxy_wstunnel,
    remoteip,
    rewrite,
    reqtimeout,
    socache_shmcb,
    unique_id,
    http2,
    setenvif,
    status,
  ].flatten

  class { 'apache':
    mpm_module        => 'event',
    error_log         => 'syslog:daemon:',
    access_log_file   => 'syslog',
    serveradmin       => 'me@igalic.co',
    default_vhost     => false,
    default_ssl_vhost => false,
    log_formats       => { vhost_combined => '%V %a %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"' },
    default_mods      => $default_mods,
  }

  apache::listen {['80', '443']: }

  apache::custom_config { 'custom-log':
    content => "CustomLog syslog vhost_combined\n",
  }
  apache::custom_config { 'protocols':
    content => "Protocols h2 http/1.1\n",
  }

  unless $mod_md.empty {
    $md = [
      'MDBaseServer on',
      'MDCertificateAgreement https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf',
      'MDRequireHttps permanent', # redirects http to https
     'ServerAdmin me@igalic.co', # set contact information
    ]
    apache::custom_config { 'configure md':
      priority => 35,
      content  => "${md.join("\n")}\n",
    }
  }

  $mdvhosts.each |$v, $d| {
    apache::vhost::custom { $v:
      priority => pick($d['priority'], '25'),
      content  => epp('profile/mdvhost.epp',
                      deep_merge($mdvhost_defaults,
                                 delete($d, "priority"),
                                 { server_name => $v })),
    }
  }
  $vhosts.each |$v, $d| {
    apache::vhost::custom { $v:
      priority => pick($d['priority'], '25'),
      content  => epp('profile/mdvhost.epp',
                      deep_merge($mdvhost_defaults,
                                 delete($d, "priority"),
                                 { server_name => $v,
                                   tls         => false})),
    }
  }
}
