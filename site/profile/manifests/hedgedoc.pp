class profile::hedgedoc {
  service { 'hedgedoc':
    ensure => running,
    enable => true,
  }
}
